# EthSwap

An instant cryptocurrency exchange full stack blockchain application built on top of Ethereum. It allows you to buy and sell cryptocurrency at fixed price

## Dependencies

```bash
Node.js
Ganache (one click blockchain)
Truffle framework (for writing smart contracts and tests) Command: npm install --g truffle@5.1.14
Metamask (To connect your browser to blockchain for the dapp to function)
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

