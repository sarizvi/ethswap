 pragma solidity ^0.5.16;

 import "./Token.sol";

 contract EthSwap {
 	string public name = "EthSwap Instant Exchange";
 	Token public token;
 	uint public rate = 100;

 	event TokenPurchased(
 	address account,
 	address token,
 	uint amount,
 	uint rate
 	);

 	event TokenSold(
 	address account,
 	address token,
 	uint amount,
 	uint rate
 	);

 	constructor(Token _token) public {
 	token = _token;

 	}

 	function buytokens() public payable {
 	uint tokenAmount = msg.value * rate;

 	require(token.balanceOf(address(this)) >= tokenAmount);

 	token.transfer(msg.sender,tokenAmount);

 	emit TokenPurchased(msg.sender,address(token),tokenAmount,rate);
 	}

 	function selltokens(uint _amount) public {

 	require(token.balanceOf(msg.sender) >= _amount);

 	uint etheramount = _amount/rate;

 	require(address(this).balance >= etheramount);

 	token.transferFrom(msg.sender,address(this), _amount);
 	msg.sender.transfer(etheramount);

 	emit TokenSold(msg.sender,address(token),_amount,rate);

 	}
 }

