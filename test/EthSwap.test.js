const Token = artifacts.require('Token')
const EthSwap = artifacts.require('EthSwap')

require('chai')
.use(require('chai-as-promised'))
.should()

function tokens(n){
	return web3.utils.toWei(n,'ether');
}

contract("EthSwap",(accounts) =>{
	let token, ethswap

	before(async() => {
		token = await Token.new()
		ethswap = await EthSwap.new(token.address)

		await token.transfer(ethswap.address,tokens('1000000'))
	})

	describe("EthSwap deployment",async() => {
		it('contract has a name', async() =>{
			const name = await ethswap.name()
			assert.equal(name,'EthSwap Instant Exchange')
		})
	})

	describe("Token deployment",async() => {
		it('contract has a name', async() =>{
			const name = await token.name()
			assert.equal(name,'DApp Token')
		})

		it('contract has tokens', async() => {
			const name = await ethswap.name()
			let balance = await token.balanceOf(ethswap.address)
			assert.equal(balance.toString(),tokens('1000000'))
		})
	})

	describe('buyTokens()', async() => {
		let result
		before(async() => {
			result = await ethswap.buytokens({from:accounts[1],value:tokens('1')})
	})
		it("Allows user to buy tokens", async() => {
			let investorBalance = await token.balanceOf(accounts[1])
			assert.equal(investorBalance.toString(),tokens('100'))

			let ethswapbalance
			ethswapbalance = await token.balanceOf(ethswap.address)
			assert.equal(ethswapbalance.toString(),tokens('999900'))
			ethswapbalance = await web3.eth.getBalance(ethswap.address)
			assert.equal(ethswapbalance.toString(),web3.utils.toWei('1','Ether'))

			// console.log(result.logs)
			const event = result.logs[0].args
			assert.equal(event.account, accounts[1])
			assert.equal(event.token, token.address)
			assert.equal(event.amount.toString(),tokens('100').toString())
			assert.equal(event.rate.toString(),'100')
		})
	})


	describe('sellTokens()', async() => {
		let result
		before(async() => {
			await token.approve(ethswap.address,tokens('100'),{from:accounts[1]})
			result = await ethswap.selltokens(tokens('100'),{from:accounts[1]})
	})
		it("Allows user to sell tokens", async() => {
			let investorBalance = await token.balanceOf(accounts[1])
			assert.equal(investorBalance.toString(),tokens('0'))

			let ethswapbalance
			ethswapbalance = await token.balanceOf(ethswap.address)
			assert.equal(ethswapbalance.toString(),tokens('1000000'))
			ethswapbalance = await web3.eth.getBalance(ethswap.address)
			assert.equal(ethswapbalance.toString(),web3.utils.toWei('0','Ether'))

			// console.log(result.logs)
			const event = result.logs[0].args
			assert.equal(event.account, accounts[1])
			assert.equal(event.token, token.address)
			assert.equal(event.amount.toString(),tokens('100').toString())
			assert.equal(event.rate.toString(),'100')

			await ethswap.selltokens(tokens('500'),{from:accounts[1]}).should.be.rejected;
		})
	})
})